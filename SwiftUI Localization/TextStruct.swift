//
//  TextStruct.swift
//  SwiftUI Localization
//
//  Created by Yulibar Husni on 24/08/22.
//

struct AppText {
    
    // Enum
    enum Fruit: String, Hashable, CaseIterable {
        case mango, banana, orange, grape, papaya
    }
    
    enum ViewTitle: String {
        case listOfFruit = "List of fruit"
        case survey = "Survey"
    }
    
}
