//
//  SwiftUI_LocalizationApp.swift
//  SwiftUI Localization
//
//  Created by Yulibar Husni on 24/08/22.
//

import SwiftUI

@main
struct SwiftUI_LocalizationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
