//
//  ContentView.swift
//  SwiftUI Localization
//
//  Created by Yulibar Husni on 24/08/22.
//

import SwiftUI

struct ContentView: View {

    var body: some View {
        NavigationView{
            List(AppText.Fruit.allCases, id: \.self) { fruit in
                Text(LocalizedStringKey(fruit.rawValue))
                    .autocapitalization(.sentences)
                    .accessibilityLabel(fruit.rawValue)
            }
            .navigationTitle(LocalizedStringKey(AppText.ViewTitle.listOfFruit.rawValue))
            .accessibilityElement(children: /*@START_MENU_TOKEN@*/.contain/*@END_MENU_TOKEN@*/)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        ContentView()
            .environment(\.locale, .init(identifier: "id"))
    }
}
