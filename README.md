
#  SwiftUI Localization Example

*A small project to test a method for localizing app in SwiftUI*

For this localization method, I created a enum for the strings I will use in my app. And then on the SwiftUI View, I access the enum when I need to use the strings. The enum will act as a "bridge" between SwiftUI View and localized String file.

**Method**:
1. Create enum to list all the key for the String file
2. Create Strings file (in this example, we use `Localizable.strings`)
3. Setup the localization String file for other language (in this example, I use Indonesian). You can follow this tutorial `https://developer.apple.com/documentation/xcode/adding-support-for-languages-and-regions`
4. Connect the key in enum file to the Strings file. You can follow this tutorial: `https://developer.apple.com/documentation/swiftui/preparing-views-for-localization`
5. Start building the String in the Strings file
6. Call the key in the SwiftUI View via enum using `LocalizedStringKey(AppText.ViewTitle.listOfFruit.rawValue)`. You can follow this tutorial: `https://developer.apple.com/documentation/swiftui/localizedstringkey` focus on first paragraph.
7. Translate the contents from Strings file.
8. Enjoy multiple language support!
##

> This is just one way to localize our SwiftUI App. There will be a
> better way out there that I still don't know, so please keep exploring!

> *With love, Yus*
